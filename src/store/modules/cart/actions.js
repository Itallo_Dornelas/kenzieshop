export const addToCart = (product) => ({ type: "@cart/ADD", product });

export const removeToCart = (newProduct) => ({
  type: "@cart/REMOVE",
  newProduct,
});
