import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";

// Uma forma diferente de combinar os reducers e colocar na store.
import rootReducer from "./modules/rootReducer";

const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;
