import { Button } from "@material-ui/core";
import { useDispatch } from "react-redux";
import products from "../../services/products";
import { Container, ProductList, Title } from "./styles";
import formatValue from "../../utils/formatValue";
import { addToCartThunk } from "../../store/modules/cart/thunks";

function Home() {
  const dispatch = useDispatch();

  return (
    <div>
      <Title>Os melhores celulares e os melhores preços!!</Title>
      <Container>
        <ProductList>
          {products.map((product) => (
            <li key={product.id}>
              <figure>
                <img src={product.image} alt={product.name} />
              </figure>
              <h2>{product.title}</h2>
              <div>
                <span>{formatValue(product.price)}</span>
                <Button onClick={() => dispatch(addToCartThunk(product))}>
                  <span>Adicionar ao Carrinho</span>
                </Button>
              </div>
            </li>
          ))}
        </ProductList>
      </Container>
    </div>
  );
}
export default Home;
