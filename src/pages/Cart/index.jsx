import React from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { Container, Image, CardContainer, ContainerNull } from "./styles";
import formatValue from "../../utils/formatValue";

import { useHistory } from "react-router-dom";
import { removeToCartThunk } from "../../store/modules/cart/thunks";

function Cart() {
  const cart = useSelector((state) => state.cart);
  const dispatch = useDispatch();

  const history = useHistory();

  const subtotal = formatValue(
    cart.reduce((product, acc) => acc.price + product, 0).toFixed(2)
  );
  const redirect = () => {
    history.push("/dashboard");
  };

  if (!cart.length) {
    return (
      <ContainerNull>
        <h1> Carrinho vazio, vamos as compras?</h1>
        <button onClick={() => history.push("/")}>Voltar aos Produtos</button>
      </ContainerNull>
    );
  }

  return (
    <>
      <Container>
        <div>
          <div>
            <h3>Produto</h3>
            <h3>Preço</h3>
          </div>
          <section>
            {cart.map((product) => (
              <div key={product.title}>
                <Image src={product.image} alt="Produto" />
                <h4>{product.title}</h4>
                {formatValue(product.price)}

                <button onClick={() => dispatch(removeToCartThunk(product.id))}>
                  Remover do Carrinho
                </button>
              </div>
            ))}
          </section>
        </div>
        <section className="sectionPedidos">
          <div>
            <h2>
              <strong>Resumo do pedido</strong>
            </h2>
            <CardContainer>
              <h4>{cart.length} Produtos</h4>
              <h4>{subtotal}</h4>
            </CardContainer>
          </div>
          <div>
            <button onClick={redirect}>Finalizar o pedido</button>
          </div>
        </section>
      </Container>
    </>
  );
}

export default Cart;
