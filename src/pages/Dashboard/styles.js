import styled from "styled-components";
export const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 10%;
  align-items: center;
  justify-content: center;
  h1 {
    color: var(--gray);
  }
  button {
    margin-top: 5px;
    background: var(--blue);
    color: var(--vanilla);
    border: 0;
    border-radius: 4px;
    display: flex;
    align-items: center;
    transition: 200ms ease-in-out;
    padding: 5px;
    font-size: 1.5rem;
    flex: 1;
    text-align: center;
    font-weight: bold;

    :hover {
      background-color: #224959;
    }
  }
`;
