import React from "react";
import Home from "../pages/Home";
import Cart from "../pages/Cart";
import Dashboard from "../pages/Dashboard";

import { Switch, Route } from "react-router-dom";

const Routes = () => {
  return (
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/cart" component={Cart} />
      <Route path="/dashboard" component={Dashboard} />
    </Switch>
  );
};

export default Routes;
