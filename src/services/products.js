const products = [
  {
    id: 1,
    title: "iPhone 12 Pro Max Apple Grafite",
    description:
      "Memória Interna: 128GB processador: Chip A14 Bionic com Nova geração do Neural Engine",
    price: 8490.0,
    image:
      "https://images-na.ssl-images-amazon.com/images/I/31ToeN0KIZL._AC_.jpg",
  },
  {
    id: 2,
    title: "Iphone 12 Pro Apple Azul-pacífico",
    description:
      "Processador MediaTek Helio G25 Octa-Core de 2GHz com 2GB de RAM.",
    price: 8470.0,
    image:
      "https://images-na.ssl-images-amazon.com/images/I/51EzMcraluL._AC_SL1080_.jpg",
  },
  {
    id: 3,
    title: "Iphone Xr Apple (product) Vermelho",
    description:
      "Processador MediaTek Helio G25 Octa-Core de 2GHz com 2GB de RAM.",
    price: 4973.14,
    image:
      "https://images-na.ssl-images-amazon.com/images/I/41kkrYAF8OL._AC_.jpg",
  },
  {
    id: 4,
    title: "Samsung Galaxy A01 Dual SIM 32 GB preto 2 GB RAM",
    description: "Processador Snapdragon 439 Octa-Core de 2GHz com 2GB de RAM.",
    price: 699.99,
    image:
      "https://http2.mlstatic.com/D_NQ_NP_661764-MLA44282592265_122020-O.webp",
  },
  {
    id: 5,
    title: "Xiaomi Redmi 9A Dual SIM 32 GB azul 2 GB RAM",
    description:
      "Processador MediaTek Helio G25 Octa-Core de 2GHz com 2GB de RAM.",
    price: 799.99,
    image:
      "https://http2.mlstatic.com/D_NQ_NP_958898-MLA43824398273_102020-O.webp",
  },
  {
    id: 6,
    title: "LG K11+ Dual SIM 32 GB dourado 3 GB RAM",
    description:
      "Processador MediaTek Helio G25 Octa-Core de 2GHz com 2GB de RAM.",
    price: 859.1,
    image:
      "https://http2.mlstatic.com/D_NQ_NP_633672-MLA31348110302_072019-O.webp",
  },
  {
    id: 7,
    title: "Motorola Moto E6s Azul Marinho",
    description: "Processador Snapdragon 439 Octa-Core de 2GHz com 2GB de RAM.",
    price: 1059.0,
    image:
      "https://images-na.ssl-images-amazon.com/images/I/51Q9CrVuiMS._AC_SL1000_.jpg",
  },
  {
    id: 8,
    title: "Motorola Moto E7 Power Azul Metalico",
    description:
      "Processador MediaTek Helio G25 Octa-Core de 2GHz com 2GB de RAM.",
    price: 901.42,
    image:
      "https://images-na.ssl-images-amazon.com/images/I/61j74H-52DL._AC_SL1000_.jpg",
  },
  {
    id: 9,
    title: "LG K22",
    description:
      "Processador MediaTek Helio G25 Octa-Core de 2GHz com 2GB de RAM.",
    price: 736.16,
    image:
      "https://images-na.ssl-images-amazon.com/images/I/61wAXVLTqiL._AC_SL1500_.jpg",
  },
  {
    id: 10,
    title: "Samsung Galaxy A01 Dual SIM 32 GB preto 2 GB RAM",
    description: "Processador Snapdragon 439 Octa-Core de 2GHz com 2GB de RAM.",
    price: 699.99,
    image:
      "https://http2.mlstatic.com/D_NQ_NP_661764-MLA44282592265_122020-O.webp",
  },
  {
    id: 11,
    title: "Xiaomi Poco",
    description:
      "Processador MediaTek Helio G25 Octa-Core de 2GHz com 2GB de RAM.",
    price: 1314.0,
    image:
      "https://images-na.ssl-images-amazon.com/images/I/61Jy7mYN78L._AC_SL1001_.jpg",
  },
  {
    id: 12,
    title: "LG K11+ Dual SIM 32 GB dourado 3 GB RAM",
    description:
      "Processador MediaTek Helio G25 Octa-Core de 2GHz com 2GB de RAM.",
    price: 859.1,
    image:
      "https://http2.mlstatic.com/D_NQ_NP_633672-MLA31348110302_072019-O.webp",
  },
  {
    id: 13,
    title: "Samsung Galaxy A01 Dual SIM 32 GB preto 2 GB RAM",
    description: "Processador Snapdragon 439 Octa-Core de 2GHz com 2GB de RAM.",
    price: 699.99,
    image:
      "https://http2.mlstatic.com/D_NQ_NP_661764-MLA44282592265_122020-O.webp",
  },
  {
    id: 14,
    title: "Xiaomi Redmi 9A Dual SIM 32 GB azul 2 GB RAM",
    description:
      "Processador MediaTek Helio G25 Octa-Core de 2GHz com 2GB de RAM.",
    price: 799.99,
    image:
      "https://http2.mlstatic.com/D_NQ_NP_958898-MLA43824398273_102020-O.webp",
  },
  {
    id: 15,
    title: "LG K11+ Dual SIM 32 GB dourado 3 GB RAM",
    description:
      "Processador MediaTek Helio G25 Octa-Core de 2GHz com 2GB de RAM.",
    price: 859.1,
    image:
      "https://http2.mlstatic.com/D_NQ_NP_633672-MLA31348110302_072019-O.webp",
  },
  {
    id: 16,
    title: "Samsung Galaxy A01 Dual SIM 32 GB preto 2 GB RAM",
    description: "Processador Snapdragon 439 Octa-Core de 2GHz com 2GB de RAM.",
    price: 699.99,
    image:
      "https://http2.mlstatic.com/D_NQ_NP_661764-MLA44282592265_122020-O.webp",
  },
  {
    id: 17,
    title: "Xiaomi Redmi 9A Dual SIM 32 GB azul 2 GB RAM",
    description:
      "Processador MediaTek Helio G25 Octa-Core de 2GHz com 2GB de RAM.",
    price: 799.99,
    image:
      "https://http2.mlstatic.com/D_NQ_NP_958898-MLA43824398273_102020-O.webp",
  },
  {
    id: 18,
    title: "LG K11+ Dual SIM 32 GB dourado 3 GB RAM",
    description:
      "Processador MediaTek Helio G25 Octa-Core de 2GHz com 2GB de RAM.",
    price: 859.1,
    image:
      "https://http2.mlstatic.com/D_NQ_NP_633672-MLA31348110302_072019-O.webp",
  },
  {
    id: 19,
    title: "Samsung Galaxy A01 Dual SIM 32 GB preto 2 GB RAM",
    description: "Processador Snapdragon 439 Octa-Core de 2GHz com 2GB de RAM.",
    price: 699.99,
    image:
      "https://http2.mlstatic.com/D_NQ_NP_661764-MLA44282592265_122020-O.webp",
  },
  {
    id: 20,
    title: "Xiaomi Redmi 9A Dual SIM 32 GB azul 2 GB RAM",
    description:
      "Processador MediaTek Helio G25 Octa-Core de 2GHz com 2GB de RAM.",
    price: 799.99,
    image:
      "https://http2.mlstatic.com/D_NQ_NP_958898-MLA43824398273_102020-O.webp",
  },
  {
    id: 21,
    title: "LG K11+ Dual SIM 32 GB dourado 3 GB RAM",
    description:
      "Processador MediaTek Helio G25 Octa-Core de 2GHz com 2GB de RAM.",
    price: 859.1,
    image:
      "https://http2.mlstatic.com/D_NQ_NP_633672-MLA31348110302_072019-O.webp",
  },
  {
    id: 22,
    title: "Samsung Galaxy A01 Dual SIM 32 GB preto 2 GB RAM",
    description: "Processador Snapdragon 439 Octa-Core de 2GHz com 2GB de RAM.",
    price: 699.99,
    image:
      "https://http2.mlstatic.com/D_NQ_NP_661764-MLA44282592265_122020-O.webp",
  },
  {
    id: 23,
    title: "Xiaomi Redmi 9A Dual SIM 32 GB azul 2 GB RAM",
    description:
      "Processador MediaTek Helio G25 Octa-Core de 2GHz com 2GB de RAM.",
    price: 799.99,
    image:
      "https://http2.mlstatic.com/D_NQ_NP_958898-MLA43824398273_102020-O.webp",
  },
  {
    id: 24,
    title: "LG K11+ Dual SIM 32 GB dourado 3 GB RAM",
    description:
      "Processador MediaTek Helio G25 Octa-Core de 2GHz com 2GB de RAM.",
    price: 859.1,
    image:
      "https://http2.mlstatic.com/D_NQ_NP_633672-MLA31348110302_072019-O.webp",
  },
];

export default products;
